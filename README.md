[![docs-master](https://img.shields.io/badge/docs-master-blue)](https://cge.frama.io/pyrgrow/)

Repository for PyRgrow.

To install the most recent tagged version, you can use the following:

```sh
pip install -U pyrgrow --index-url https://framagit.org/api/v4/projects/67113/packages/pypi/simple
```

The [latest master-branch wheels](https://framagit.org/cge/pyrgrow/-/jobs/artifacts/master/browse/target/wheels?job=build) are also auto-generated..
